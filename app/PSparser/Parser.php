<?php
/**
 * Created by PhpStorm.
 * User: belte
 * Date: 30.11.2018
 * Time: 07:04
 */

namespace App\PSparser;


use App\PSparser\Interfaces\PSParser;

class Parser
{
    protected $ps_parser;

    public function __construct(PSParser $ps_parser)
    {
        $this->ps_parser = $ps_parser;

    }

    public function parseHtml($html_code){
        return $this->ps_parser->parse();
    }
}
