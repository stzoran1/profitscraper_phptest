<?php
/**
 * Created by PhpStorm.
 * User: belte
 * Date: 30.11.2018
 * Time: 06:53
 */

namespace App\PSparser\Interfaces;

interface PSParser
{
    //Define methods which must be present in class
    public function __construct($html_code);

    //Returns array of available information
    public function parse();

    //Returns product title as string
    public function getProductTitle();

    //Returns inner HTML of product details
    public function getProductDetails();

    //Returns product SKU if available
    public function getProductSKU();

    //Returns product Brand name if available
    public function getProductBrand();

    //Returns source URL of product main image
    public function getProductMainImage();

    //Returns array of source URLs of additional images, excluding main image
    public function getProductAdditionalImages();

    //Returns array of product price and price currency
    public function getProductPrice();

    //Returns array of product variations if available
    public function getProductVariations();

    //Returns array of product specifications if available
    public function getProductSpecifications();

    //Returns product stock status
    //0 - not in stock
    //1 - in stock
    public function getProductStockStatus();

    //Returns reason if out of stock if available
    public function getProductStockRemark();
}
