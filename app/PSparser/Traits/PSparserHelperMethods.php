<?php
/**
 * Created by PhpStorm.
 * User: belte
 * Date: 30.11.2018
 * Time: 08:37
 */

namespace App\PSparser\Traits;


use GuzzleHttp\Client;

trait PSparserHelperMethods
{
    public function removeQueryString($url)
    {
        return explode('?', $url)[0];
    }

    public function decodeHtmlSpecials($string)
    {
        return str_replace("&quot;", '"', htmlspecialchars_decode($string));
    }

    public function ajaxGetJson($url)
    {
        $client = new Client();
        $response = $client->request('GET', $url);

        return [
            "status" => $response->getStatusCode(),
            "data" => json_decode($response->getBody(),true)
        ];
    }
}
