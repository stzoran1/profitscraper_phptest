<?php
/**
 * Created by PhpStorm.
 * User: belte
 * Date: 27.11.2018
 * Time: 13:32
 */

namespace App\PSparser\WebSites;

use App\PSparser\Interfaces\PSParser;
use App\PSparser\Traits\PSparserHelperMethods;
use PHPHtmlParser\Dom;

class BedBathAndBeyond implements PSParser
{
    use PSparserHelperMethods;

    protected $dom;
    protected $json_data;

    //Make specific parser for each web site depending of content/architecture

    public function __construct($html_code)
    {
        $this->dom = new Dom;
        $this->dom->load($html_code);
        $this->json_data = null;
    }

    public function parse()
    {
        return [
            "product_sku" => $this->getProductSKU(),
            "product_brand" => $this->getProductBrand(),
            "product_title" => $this->getProductTitle(),
            "product_details" => $this->getProductDetails(),
            "product_main_image" => $this->getProductMainImage(),
            "product_additional_images" => $this->getProductAdditionalImages(),
            "product_price" => $this->getProductPrice(),
            "product_variations" => $this->getProductVariations(),
            "product_specifications" => $this->getProductSpecifications(),
            "product_stock" => $this->getProductStockStatus(),
            "product_stock_remark" => $this->getProductStockRemark()
        ];
    }

    public function getProductTitle()
    {
        $product_title = $this->dom->find("[data-locator='pdp-productnametext']");
        return $product_title = $product_title->text;
    }

    //This differ from SKU and it is used for making AJAX call to get data
    protected function getProductNumber()
    {
        $href = $this->dom->find("[itemprop='url']")[1]->getAttribute('href');
        $href = explode("/", $href);

        return $href[(count($href) - 1)];
    }

    public function getProductDetails()
    {
        return $this->getProductDetailsDiv()->innerHtml;
    }

    protected function getProductDetailsDiv()
    {
        //Product details are inside this div
        return $this->dom->find("[id='productDetails']");
    }

    public function getProductSKU()
    {
        //Product SKU is located in li of product details
        $product_details_containers = $this->getProductDetailsDiv()->find('li');

        $product_sku = "N/A";

        foreach ($product_details_containers as $item) {
            if (strpos($item->text, "sku") !== False && $product_sku == "N/A") {
                $product_sku = trim(explode(":", $item->text)[1]);
            }
        }

        return $product_sku;
    }

    public function getProductBrand()
    {
        //Brand (not available for all products)
        try {
            $brand_container = $this->dom->find("[itemprop='brand']");
            $product_brand = $brand_container->find("span")->text;
        } catch (\Exception $ex) {
            $product_brand = "N/A";
        }

        return $product_brand;
    }

    public function getProductMainImage()
    {
        //Main image link
        $product_main_image = $this->dom->find('img.ProductMediaCarouselStyle_180Eek');
        $product_main_image_link = $product_main_image[0]->getAttribute('src');
        //Remove query string if any
        return $this->removeQueryString($product_main_image_link);
    }

    public function getProductAdditionalImages()
    {
        //Additional images
        $product_additional_images = $this->dom->find('img.ProductMediaCarouselStyle_NDjUwY');
        $additional_images = [];
        $product_main_image_link = $this->getProductMainImage();
        foreach ($product_additional_images as $image) {
            //Remove query string if any
            $src = $this->removeQueryString($image->getAttribute('src'));
            //Prevent duplicating main and additional images
            if ($src != $product_main_image_link) {
                $additional_images[] = $src;
            }
        }
        //Remove duplicated images:
        return array_unique($additional_images);
    }

    public function getProductPrice()
    {
        //Price
        try {
            //Try to find unique price
            $price_main = floatval($this->dom->find("[itemprop='price']")->getAttribute("content"));
            $price_main_currency = $this->dom->find("[itemprop='priceCurrency']")->getAttribute("content");
            $price = ["price" => $price_main, "currency" => $price_main_currency];
        } catch (\Exception $ex) {
            //If upper failed then search for price range
            $price_main_low = floatval($this->dom->find("[itemprop='lowPrice']")->getAttribute("content"));
            $price_main_high = floatval($this->dom->find("[itemprop='highPrice']")->getAttribute("content"));
            $price_main_currency = $this->dom->find("[itemprop='priceCurrency']")->getAttribute("content");
            $price = ["low_price" => $price_main_low, "high_price" => $price_main_high, "currency" => $price_main_currency];
        }

        return $price;
    }

    //Used to set json_data variable in order to avoid making multiple calls to API if not needed
    protected function getJsonData($force = false)
    {
        //Prevent calling API if json_data is set and if force is false
        if ($force == true || $this->json_data == null) {
            $this->json_data = $this->ajaxGetJson('https://www.bedbathandbeyond.com/apis/stateless/v1.0/sku/product?product=' . $this->getProductNumber());
        }
    }

    public function getProductVariations()
    {
        //Variations (not available for all products)
        $this->getJsonData();
        $data = $this->json_data['data']['data'];
        //If data has more then 1 member then we have variations
        //Otherwise we have only data about existing product
        if (count($data) > 1) {
            return $data;
        } else {
            return $data;
        }
    }

    public function getProductSpecifications()
    {
        //Specifications (not available for all products)
        $specifications = [];
        try {
            $spec_container_div = $this->dom->find("[id='specs']");
            $specs_cells = $spec_container_div->find("[class^='cell Specs']");

            foreach ($specs_cells as $spec) {
                //Find inner divs containing data
                $specs = $spec->find("[class^='cell Specs']");
                $specifications[$specs[0]->text] = $specs[1]->text;
            }
        } catch (\Exception $ex) {
        }

        return $specifications;
    }

    public function getProductStockStatus()
    {
        //Stock availability
        $stock = 0;
        try {
            $available_container = $this->dom->find("[itemprop='availability']");

            if (strpos($available_container->getAttribute("href"), "InStock") !== false) {
                $stock = 1;
            }
        } catch (\Exception $ex) {
        }
        return $stock;
    }

    public function getProductStockRemark()
    {
        //Out of stock remark
        try {
            $out_of_stock_div = $this->dom->find("[class='OutOfStock_248A1m mb2']");
            $out_of_stock_remark = $out_of_stock_div->find('p')->text;
        } catch (\Exception $ex) {
            $out_of_stock_remark = "N/A";
        }

        return $out_of_stock_remark;
    }
}
