<?php

namespace App\Http\Controllers;

use App\PSparser\WebSites\BedBathAndBeyond;
use App\PSparser\Parser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestParsingController extends Controller
{
    //
    public function __construct()
    {
        //This is required for PHP version 7.2 to prevent count() error to pop up
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
    }

    public function index($id = 0)
    {
        //For testing purpose test page content is saved in storage/app/example.html
        $html_code = Storage::get("example$id.html");

        $ps_parser = new Parser(new BedBathAndBeyond($html_code));

        return $ps_parser->parseHtml($html_code);
    }

}
